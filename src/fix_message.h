#ifndef FIX_MESSAGE_H
#define FIX_MESSAGE_H

#include <unordered_map>
#include <list>
#include <vector>

#include <rapidjson/document.h>

/*args = { 
 * original_message,
 * generated_message_type,
 * text,
 * tag
 * session_reject_reason }
 */

using namespace rapidjson;

namespace fix {

class FIXMessages {
public:
    typedef void (FIXMessages::*func_pointer)(std::string&);
    std::unordered_map<std::string, func_pointer> function_list;
    void initialize();
    std::string generate_message(const std::string& orig_msg, const Document& msg_values);
private:
    bool add_orig(const std::string& tag, std::string& reply);
    bool add_from_msg_values(const std::string& tag, std::string& reply);
    void add_8(std::string& reply);
    void add_9(std::string& reply);
    void add_10(std::string& reply);
    void add_34(std::string& reply);
    void add_45(std::string& reply);
    void add_49(std::string& reply);
    void add_52(std::string& reply);
    void add_56(std::string& reply);
    void add_372(std::string& reply);
    void add_tag_value(std::string& msg, const std::string& tag, const std::string& value);
    bool add_original_tag_value(std::string& msg, const std::string& tag, const std::string& original_message);
    void initialize_functions();
    void call_function(const std::string& function_name, std::string& reply);
    void generate_section(const std::string& section, std::string& reply);
    void add_length(std::string& msg);
    void generate_arg_vector(const std::list<std::string>& args);

    std::vector<std::string> arg_vector_;
    Document config_;
    Document user_config_;
    Document fix_config_;
    Document msg_values_;
    std::string sendercompid_;
    std::string orig_msg_;
};

} // namespace fix


#endif // FIX_MESSAGE_H
