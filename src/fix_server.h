#ifndef FIX_SERVER_H
#define FIX_SERVER_H

#include <queue>
#include <map>

#include <mix/server.h>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

#include "fix_login_handler.h"
#include "fix_message.h"
#include "fix_validate.h"
#include "orderbook.h"

using namespace rapidjson;

void my_handler(int s);
static std::string store;

class FIXServer : public mix::sockets::Server
{
public:
    void on_server_message_in(const std::string& msg, const int& fd) override;
    void on_server_message_out(const std::string& message, const int& fd) override;
    void on_disconnection(const int& fd) override;
    void my_handler(int s);
    void initialize();
private:
    std::string get_tag_value(const std::string& msg, const std::string tag);
    int get_checksum_int(const std::string& s);
    void right_pad(std::string& s, int padded_len, const char& pad_char);
    bool key_exists(const Document& d, const std::string& key);
    void increase_incoming_seq_num(const std::string& msg);
    void send_heartbeats();
    void process_message(const std::string& msg, const int fd);
    void process_messages(const std::string& msg, const int fd);
    void process_order_queue();

    Document config_;
    Document user_config_;
    Document fix_config_;
    fix::Validater validater_;
    fix::LoginHandler login_handler_;
    fix::FIXMessages fixm_;
    std::queue<std::string> order_queue_;
    Orderbook book_;
};

#endif
