#ifndef FIX_LOGIN_HANDLER_H
#define FIX_LOGIN_HANDLER_H

#include <rapidjson/document.h>

#include "fix_message.h"
#include "fix_validate.h"

using namespace rapidjson;

namespace fix {

class LoginHandler {
public:
    void initialize();
    void disconnect(const int fd);
    void manage_login(const std::string& msg, const int fd);
    void check_login_status(const std::string& msg);
private:
    void initialize_login_map();
    void login(const std::string& msg, const int fd);
    void logout(const std::string& msg, const int fd);
    void erase_login_details(const int fd);
    void erase_user(const int fd);
    void erase_socket(const int fd);
    Document config_;
    Document user_config_;
    fix::Validater validater_;
    fix::FIXMessages fix_;
    std::map<int, std::string> socket_map_;
};

} // end namespace fix

#endif
