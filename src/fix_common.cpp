#include "fix_common.h"

#include <mix/globals.h>
#include <mix/sockets.h>
#include <mix/utility.h>

#include "fix_globals.h"
#include "fix_utility.h"

namespace fix {

void send_message(const std::string& reply, const int& fd, const bool increment_in_seq_no)
{
    auto targetcompid = fix::get_tag_value(reply, "56");
    if (increment_in_seq_no) {
        seq_map[targetcompid].incoming++;
    }
    seq_map[targetcompid].outgoing++;
    if (!reply.empty()) {
        mix::sockets::send_bytes(fd, (void*)(reply.c_str()), reply.size());
        auto modified = reply;
        mix::strings::replace_all(modified, "\x01", "|");
        logger.add_log(LOG_DEBUG, "Sent message: " + modified + "\n", 1);
    }
}

} // end namespace fix
