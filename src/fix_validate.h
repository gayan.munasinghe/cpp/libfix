#ifndef VALIDATER_H
#define VALIDATER_H

#include <map>

#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

using namespace rapidjson;

namespace fix {

class Validater {

public:
    void validate(const std::string& msg);
    void initialize();
private:
    void validate_tags(const std::string& msg);
    void validate_section_tags(const std::string&msg, const std::string& section);
    void validate_header_tags(const std::string& msg);
    void validate_body_tags(const std::string& msg);
    void validate_trailer_tags(const std::string& msg);
    void validate_checksum(const std::string& msg);
    void validate_version(const std::string& msg);
    void validate_body_length(const std::string& msg);
    void validate_sendercompid(const std::string& msg);
    void validate_targetcompid(const std::string& msg);
    void validate_username(const std::string& msg);
    void validate_password(const std::string& msg);
    void validate_sequence_number(const std::string& msg);
    Document config_;
    Document user_config_;
    Document fix_config_;
};

} // end nampesapce fix

#endif
