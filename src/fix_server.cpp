#define RAPIDJSON_HAS_STDSTRING 1

#include "fix_server.h"

#include <chrono>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iostream>
#include <mutex>
#include <regex>
#include <thread>

#include <mix/exception.h>
#include <mix/globals.h>
#include <mix/log.h>
#include <mix/utility.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <rapidjson/pointer.h>

#include "fix_common.h"
#include "fix_exception.h"
#include "fix_globals.h"
#include "fix_utility.h"

#define SOH "\x01"

void my_handler(int s)
{
    std::ofstream myfile;
    logger.add_log(LOG_ERR, "Caught signal " + std::to_string(s) + "\n");
    auto epoch = mix::times::get_epoch();
    auto date = mix::times::get_datetime_from_epoch(epoch, "%Y%m%d");
    for (auto it : seq_map) {
        myfile.open (store + "/" + it.first + ".in." + date);
        myfile << it.second.incoming << std::endl;
        myfile.close();
        myfile.open (store + "/" + it.first + ".out." + date);
        myfile << it.second.outgoing << std::endl;
        myfile.close();
    }
    exit(1);
}

void FIXServer::initialize()
{
    mix::json::read_config(mix_config_file, config_);
    mix::json::read_config(mix::json::get_string(config_, "/users"), user_config_);
    mix::json::read_config(mix::json::get_string(config_, "/protocol"), fix_config_);
    store = mix::json::get_string(config_, "/store");
    logger.add_log(LOG_DEBUG, "sendercompid " + mix::json::get_string(config_, "/sendercompid") + "\n");
    validater_.initialize();
    login_handler_.initialize();
    fixm_.initialize();
    std::thread t(&FIXServer::send_heartbeats, this);
    t.detach();
    std::thread t2(&FIXServer::process_order_queue, this);
    t2.detach();
}

void FIXServer::on_disconnection(const int& fd)
{
    login_handler_.disconnect(fd);
}

void FIXServer::increase_incoming_seq_num(const std::string& msg)
{
    auto sendercompid = fix::get_tag_value(msg, "49");
    login_map_[sendercompid].last_message_time = std::stoi(mix::times::get_epoch());
    seq_map[sendercompid].incoming++;
}

void FIXServer::process_message(const std::string& msg, const int fd)
{
    auto message_type = fix::get_tag_value(msg, "35");

    // First, if the session is logged in and a login request is sent or if the
    // session is not logged in and a message other than a login request is sent
    // it will be handled immediately. Errors thrown by a login request should not
    // send replies. In that case return immediately.
    try {
        login_handler_.check_login_status(msg);
        validater_.validate(msg);
    } catch (const mix::fix::fix_reject& e) {
        if ("A" != message_type) {
            throw;
        } else {
            logger.add_log(LOG_ERR, std::string(e.where()) + " " + e.what() + " (tag " + e.tag() +")\n");
            return;
        }
    }

    // If the validation passes increase the incoming sequence number??
    increase_incoming_seq_num(msg);

    if ("A" == message_type || "5" == message_type) {
        login_handler_.manage_login(msg, fd);
    }
    else if ("D" == message_type || "F" == message_type || "G" == message_type) {
        order_queue_.push(msg);
    }
}

void FIXServer::process_messages(const std::string& msgs, const int fd)
{
    // recursively reads all FIX messages in the string 'msg'.
    // "(8=.*?10=.*?\x01)(.*)" has two capture groups. First group captures the
    // first FIX message and the next group captures the rest of the message.
    // second group is fed back to the function recursively until all messages
    // are processed.
    std::string reg_search = std::string("(8=.*?10=.*?") + SOH + ")(.*)";
    std::regex rgx(reg_search);
    std::smatch matches;
    std::regex_search(msgs, matches, rgx);
    process_message(matches[1].str(), fd);
    if (!matches[2].str().empty()) {
        process_messages(matches[2].str(), fd);
    }
}

void FIXServer::on_server_message_in(const std::string& msg, const int& fd)
{
    auto received_message = msg;
    received_message.erase(received_message.find('\0')); // remove all null characters.
    auto printing_message = received_message;
    std::string reply{""};
    try {
        mix::strings::replace_all(printing_message, "\x01", "|");
        logger.add_log(LOG_DEBUG, "Recieved message: " + printing_message + "\n", 1);
        process_messages(received_message, fd);
    }
    catch (const mix::key_error& e) {
        logger.add_log(LOG_ERR, std::string(e.where()) + " : key_error : key " + e.what() +"\n");
    }
    catch (const mix::fix::reject_3_sequence_no& e) {
        logger.add_log(LOG_ERR, std::string(e.where()) + " " + e.what() + "(" + e.tag() +")\n");
        increase_incoming_seq_num(received_message);
        Document doc;
        SetValueByPointer(doc, "/35", "3");
        SetValueByPointer(doc, "/58", std::string(e.what()) + " (" + e.tag() + ")");
        SetValueByPointer(doc, "/371", e.tag());
        SetValueByPointer(doc, "/373", e.reason());
        fix::send_message(reply, fd, false);
    }
    catch (const mix::fix::reject_3& e) {
        logger.add_log(LOG_ERR, std::string(e.where()) + " " + e.what() + "(" + e.tag() +")\n");
        increase_incoming_seq_num(received_message);
        Document doc;
        SetValueByPointer(doc, "/35", "3");
        SetValueByPointer(doc, "/58", std::string(e.what()) + " (" + e.tag() + ")");
        SetValueByPointer(doc, "/371", e.tag());
        SetValueByPointer(doc, "/373", e.reason());
        reply = fixm_.generate_message(msg, doc);
        fix::send_message(reply, fd);
    }
    catch (const mix::fix::logout_response& e) {
        logger.add_log(LOG_ERR, std::string(e.where()) + " " + e.what() + "\n");
        Document doc;
        SetValueByPointer(doc, "/35", "5");
        SetValueByPointer(doc, "/58", std::string(e.what()));
        reply = fixm_.generate_message(msg, doc);
        fix::send_message(reply, fd);
    }
    catch (const mix::fix::fix_exception& e) {
        logger.add_log(LOG_ERR, std::string(e.where()) + " " + e.what() +"\n");
    }
    catch (const mix::mix_exception& e) {
        logger.add_log(LOG_ERR, std::string(e.where()) + " " + e.what() +"\n");
    }
    catch (const std::exception& e) {
        logger.add_log(LOG_ERR, std::string("Exception occured") + e.what() + "\n");
    }
}

void FIXServer::on_server_message_out(const std::string&, const int&)
{
}

void FIXServer::send_heartbeats()
{
    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        login_map_mutex.lock();
        for (auto it : login_map_) {
            if (it.second.logged_in) {
                auto current_time = std::stoi(mix::times::get_epoch());
                if ((current_time - it.second.last_message_time >= it.second.hb_interval) && (it.second.last_message_time > 0)) {
                    std::string msg{std::string(SOH) + "49=" + it.first + SOH};
                    Document doc;
                    SetValueByPointer(doc, "/35", "0");
                    auto reply = fixm_.generate_message(msg, doc);
                    fix::send_message(reply, it.second.sockfd);
                    login_map_[it.first].last_message_time = current_time;
                }
            }
        }
        login_map_mutex.unlock();
    }
}

void FIXServer::process_order_queue()
{
    while (true) {
        if (!order_queue_.empty()) {
            auto it = order_queue_.front();
            auto timestamp = mix::times::get_epoch();
            book_.process_order(it, timestamp);
            order_queue_.pop();
        }
    }
}
