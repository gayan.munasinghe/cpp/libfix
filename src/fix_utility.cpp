#include <rapidjson/document.h>


#include <iostream>
#include <fstream>

#include <mix/utility.h>

#include "fix_globals.h"
#include "fix_utility.h"

namespace fix {

#define SOH "\x01"

void reset_sequence_numbers(const std::string& session)
{
    seq_map[session].incoming = 1;
    seq_map[session].outgoing = 1;
}

int get_sequence_number(const std::string& file_name)
{
    std::string line;
    std::ifstream myfile (file_name);
    if (myfile.is_open()) {
        getline(myfile,line);
        myfile.close();
    } else {
        line = "1";
    }
    return std::stoi(line);
}

std::string get_tag_value(const std::string& msg, const std::string& tag)
{
    auto reg_search = get_match(msg, tag);
    return std::get<1>(reg_search)[1].str();
}

bool tag_exists(const std::string& msg, const std::string& tag)
{
    auto reg_search = get_match(msg, tag);
    return std::get<0>(reg_search);
}

int get_checksum_int(const std::string& s)
{
    int sum = 0;
    for (uint8_t i=0; i < s.length(); i++) {
        sum = sum + static_cast<int>(s[i]);
    }
    return sum % 256;
}

std::string get_search_string(const std::string& tag)
{
    std::string reg_search;
    if (tag == "8") {
        reg_search = tag + "=" + "(.*?)" + SOH;
    }
    else {
        reg_search = SOH + tag + "=" + "(.*?)" + SOH;
    }
    return reg_search;
}

std::tuple<bool, std::smatch> get_match(const std::string& msg, const std::string& tag)
{
    std::string reg_search{get_search_string(tag)};
    std::regex rgx(reg_search);
    std::smatch matches;
    bool result = std::regex_search(msg, matches, rgx);
    return std::make_tuple(result, matches);
}

std::string get_checksum_string(const std::string& msg, bool with_trailer)
{
    if (with_trailer) {
        std::string reg_search = std::string("(8=.*") + SOH + ")10=";
        std::regex rgx(reg_search);
        std::smatch matches;
        std::regex_search(msg, matches, rgx);
        return matches[1];
    }
    return msg;

}

std::string calculate_checksum(const std::string& msg, bool with_trailer)
{
    int check_sum_int = fix::get_checksum_int(fix::get_checksum_string(msg, with_trailer));
    std::string checksum = std::to_string(check_sum_int);
    mix::strings::left_pad(checksum, 3, '0');
    return checksum;
}

} // namespace fix
