#ifndef FIX_EXCEPTION_H
#define FIX_EXCEPTION_H

#include <string>

#include <mix/exception.h>

namespace mix {

namespace fix {

class fix_exception : public mix_exception {
public:
    fix_exception(const std::string& what, const std::string& where, const std::string& tag)
        : mix_exception(what, where)
        , tag_(tag) {
    }

    std::string tag() const {
        return tag_;
    }


private:
    std::string tag_;
};

// use to trigger a response for a particular message using an exception
class fix_response : public mix_exception {
public:
    fix_response(const std::string& what, const std::string& where, const std::string& message_type)
        : mix_exception(what, where)
        , message_type_(message_type) {
    }

    std::string message_type() const {
        return message_type_;
    }

private:
    std::string message_type_;
};

class logout_response : public fix_response {
public:
    logout_response(const std::string& what, const std::string& where, const std::string& message_type = "5")
        : fix_response(what, where, message_type) {}
};

class silent_ignore : public fix_response {
public:
    silent_ignore(const std::string& what, const std::string& where, const std::string& message_type = "5")
        : fix_response(what, where, message_type) {}
};

class fix_reject : public fix_exception {
public:
    fix_reject(const std::string& what, const std::string& where, const std::string& tag, const std::string& reason)
        : fix_exception(what, where, tag)
        , reason_(reason) {
    }

    std::string reason() const {
        return reason_;
    }

private:
    std::string reason_;
};

class reject_3 : public fix_reject {
public:
    reject_3(const std::string& what, const std::string& where, const std::string& tag, const std::string& reason)
        : fix_reject(what, where, tag, reason) {}
};

class reject_3_sequence_no : public reject_3 {
public:
    reject_3_sequence_no(const std::string& what, const std::string& where, const std::string& tag, const std::string& reason)
        : reject_3(what, where, tag, reason) {}
};

} // end namespace fix

} // end namespace mix

#endif // FIX_EXCEPTION_H
