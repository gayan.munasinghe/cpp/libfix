#include "fix_login_handler.h"

#include <iostream>
#include <fstream>

#include <mix/globals.h>
#include <mix/log.h>
#include <mix/utility.h>
#include <rapidjson/pointer.h>

#include "fix_common.h"
#include "fix_exception.h"
#include "fix_globals.h"
#include "fix_utility.h"

std::map<std::string, login> login_map_;
std::map<std::string, sequence_number> seq_map;
std::mutex login_map_mutex;

namespace fix {

void LoginHandler::initialize()
{
    mix::json::read_config(mix_config_file, config_);
    mix::json::read_config(mix::json::get_string(config_, "/users"), user_config_);
    validater_.initialize();
    fix_.initialize();
    initialize_login_map();
}


void LoginHandler::initialize_login_map()
{
    auto epoch = mix::times::get_epoch();
    auto date = mix::times::get_datetime_from_epoch(epoch, "%Y%m%d");
    for (auto& m : user_config_.GetObject()) {
        seq_map[m.name.GetString()].incoming = fix::get_sequence_number(std::string("tokens/") + m.name.GetString() + ".in." + date);
        seq_map[m.name.GetString()].outgoing = fix::get_sequence_number(std::string("tokens/") + m.name.GetString() + ".out." + date);
        logger.add_log(LOG_DEBUG, std::string("Initialized user ") + m.name.GetString() + "\n", 1);
    }
}

void LoginHandler::erase_user(const int fd)
{
    auto sendercompid = socket_map_[fd];
    auto it = login_map_.find(sendercompid);
    if (it != login_map_.end()) {
        login_map_.erase(it);
        logger.add_log(LOG_DEBUG, sendercompid + " was removed from the login map\n" , 1);
    }
}

void LoginHandler::erase_socket(const int fd)
{
    auto it2 = socket_map_.find(fd);
    if (it2 != socket_map_.end()) {
        socket_map_.erase(it2);
        logger.add_log(LOG_DEBUG, fd + " was removed from the socket map\n" , 1);
    }
}

void LoginHandler::erase_login_details(const int fd)
{
    erase_user(fd);
    erase_socket(fd);
}

void LoginHandler::disconnect(const int fd)
{
    auto sendercompid = socket_map_[fd];
    erase_login_details(fd);
    logger.add_log(LOG_DEBUG, sendercompid + " disconnected\n" , 1);
}

void LoginHandler::check_login_status(const std::string& msg)
{
    auto sendercompid = fix::get_tag_value(msg, "49");
    auto message_type = fix::get_tag_value(msg, "35");
    if (login_map_[sendercompid].logged_in) { // buggy
        if ("A" == message_type) {
            throw mix::fix::silent_ignore(sendercompid + " Already logged in", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
        }
    }
    else {
        if ("A" != message_type) {
            throw mix::fix::logout_response("Session not logged in", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
        }
    }
}

void LoginHandler::login(const std::string& msg, const int fd)
{
    auto sendercompid = fix::get_tag_value(msg, "49");
    logger.add_log(LOG_INFO, sendercompid + " Login request received.\n");
    login_map_[sendercompid].logged_in = true;
    login_map_[sendercompid].sockfd = fd;
    socket_map_[fd] = sendercompid;
    login_map_[sendercompid].hb_interval = std::stoi(fix::get_tag_value(msg, "108"));
    logger.add_log(LOG_INFO, sendercompid + " Logged in.\n");
}

void LoginHandler::logout(const std::string& msg, const int fd)
{
    auto sendercompid = fix::get_tag_value(msg, "49");
    logger.add_log(LOG_DEBUG, sendercompid + " login out\n", 1);
    login_map_[sendercompid].logged_in = false;
    erase_login_details(fd);
    logger.add_log(LOG_INFO, sendercompid + " Logged out.\n");
}

void LoginHandler::manage_login(const std::string& msg, const int fd)
{
    std::string reply;
    auto message_type = fix::get_tag_value(msg, "35");
    if ("A" == message_type) {
        login(msg, fd);
        auto reset_seq_no = fix::get_tag_value(msg, "141");
        if (reset_seq_no == "Y") {
            fix::reset_sequence_numbers(fix::get_tag_value(msg, "49"));
        }
        Document d;
        SetValueByPointer(d, "/35", "A");
        reply = fix_.generate_message(msg, d);
        fix::send_message(reply, fd);
    } else if ("5" == message_type) {
        logout(msg, fd);
        Document d;
        SetValueByPointer(d, "/35", "5");
        reply = fix_.generate_message(msg, d);
        fix::send_message(reply, fd);
        logout(msg, fd);
    }
}

} // end namespace fix
