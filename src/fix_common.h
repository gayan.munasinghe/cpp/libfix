#ifndef FIX_COMMON_H
#define FIX_COMMON_H

#include <string>

namespace fix {
    void send_message(const std::string& reply, const int& fd, const bool increment_in_seq_no=false);
} // end namespace fix

#endif // FIX_COMMON_H
