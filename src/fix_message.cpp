#include "fix_message.h"

#include <iostream>
#include <list>

#include <mix/globals.h>
#include <mix/utility.h>

#include "fix_globals.h"
#include "fix_utility.h"

#define SOH "\x01"

namespace fix {

void FIXMessages::call_function(const std::string& function_name, std::string& reply)
{
    // This function will add values for a tag as follows
    // 1. If tag is in msg_values_ use it
    // 2. If the function is defined use it (ex: add_8)
    // 3. If tag is in the original msg use it
    if (!add_from_msg_values(function_name, reply)) {
        auto it = function_list.find(function_name);
        if (it == function_list.end()) {
                add_orig(function_name, reply);
        } else {
            (this->*it->second)(reply);
        }
    }
}

void FIXMessages::initialize()
{
    mix::json::read_config(mix_config_file, config_);
    mix::json::read_config(mix::json::get_string(config_, "/users"), user_config_);
    mix::json::read_config(mix::json::get_string(config_, "/protocol"), fix_config_);
    sendercompid_ = mix::json::get_string(config_, "/sendercompid");
    initialize_functions();
}

bool FIXMessages::add_orig(const std::string& tag, std::string& reply)
{
    return add_original_tag_value(reply, tag, orig_msg_);
}

bool FIXMessages::add_from_msg_values(const std::string& tag, std::string& reply)
{
    auto value = mix::json::get_string(msg_values_, std::string("/") + tag);
    if (!value.empty()) {
        add_tag_value(reply, tag, value);
        return true;
    }
    return false;
}

void FIXMessages::initialize_functions()
{
    function_list.emplace("8", &fix::FIXMessages::add_8);
    function_list.emplace("9",  &fix::FIXMessages::add_9);
    function_list.emplace("10", &fix::FIXMessages::add_10);
    function_list.emplace("34", &fix::FIXMessages::add_34);
    function_list.emplace("45", &fix::FIXMessages::add_45);
    function_list.emplace("49", &fix::FIXMessages::add_49);
    function_list.emplace("52", &fix::FIXMessages::add_52);
    function_list.emplace("56", &fix::FIXMessages::add_56);
    function_list.emplace("372", &fix::FIXMessages::add_372);
}

void FIXMessages::generate_arg_vector(const std::list<std::string>& args)
{
    arg_vector_.clear();
    for (auto const& it : args) {
        arg_vector_.push_back(it);
    }
}

std::string FIXMessages::generate_message(const std::string& orig_msg, const Document& msg_values)
{
    std::string reply;
    orig_msg_ = orig_msg;
    mix::json::copy_document(msg_values, msg_values_); // check this. might be buggy.
    generate_section("header", reply);
    generate_section(mix::json::get_string(msg_values_, "/35"), reply);
    add_length(reply);
    generate_section("trailer", reply);
    return reply;
}

void FIXMessages::generate_section(const std::string& section, std::string& reply)
{
    logger.add_log(LOG_DEBUG, "Generating " + section + "\n", 1);
    Value::ConstMemberIterator itr = fix_config_.FindMember(section.c_str());
    if (itr != fix_config_.MemberEnd()) {
        for (auto& it : itr->value.GetObject()) {
            std::string json_pointer = std::string("/") + section + "/" + it.name.GetString() + "/required";
            if (mix::json::get_string(fix_config_, json_pointer) == "Y") {
                call_function(it.name.GetString(), reply);
            }
        }
    }
}

void FIXMessages::add_length(std::string& msg)
{
    std::string reg_search = std::string("9=") + SOH + "(.*" + SOH + ")";
    std::regex rgx(reg_search);
    std::smatch matches;
    std::regex_search(msg, matches, rgx);
    auto mlength = matches[1].length();
    mix::strings::replace_all(msg, std::string(SOH) + "9=" + SOH, std::string(SOH) + "9=" + std::to_string(mlength) + SOH);
}

void FIXMessages::add_tag_value(std::string& msg, const std::string& tag, const std::string& value)
{
    msg = msg + tag + "=" + value + SOH;
}

bool FIXMessages::add_original_tag_value(std::string& msg, const std::string& tag, const std::string& original_message)
{
    auto value = fix::get_tag_value(original_message, tag);
    if (!value.empty()) {
        add_tag_value(msg, tag, value);
        return true;
    }
    return false;
}

void FIXMessages::add_8(std::string& reply)
{
    add_tag_value(reply, "8", mix::json::get_string(config_, "/version"));
}

void FIXMessages::add_34(std::string& reply)
{
    auto sendercompid = fix::get_tag_value(orig_msg_, "49");
    std::string out_seq_no = std::to_string(seq_map[sendercompid].outgoing);
    add_tag_value(reply, "34", out_seq_no);
}

void FIXMessages::add_9(std::string& reply)
{
    add_tag_value(reply, "9", "");
}

void FIXMessages::add_45(std::string& reply)
{
    auto original_seq_no = fix::get_tag_value(orig_msg_, "34");
    add_tag_value(reply, "45", original_seq_no);
}

void FIXMessages::add_49(std::string& reply)
{
    add_tag_value(reply, "49", sendercompid_);
}

void FIXMessages::add_52(std::string& reply)
{
    auto utc = mix::times::get_gmt_time("%Y%m%d-%H:%M:%S");
    utc.resize(21); // remove micro seconds
    add_tag_value(reply, "52", utc);
}

void FIXMessages::add_56(std::string& reply)
{
    auto out_targetcompid = fix::get_tag_value(orig_msg_, "49");
    add_tag_value(reply, "56", out_targetcompid);
}

void FIXMessages::add_372(std::string& reply)
{
    auto original_msg_type = fix::get_tag_value(orig_msg_, "35");
    add_tag_value(reply, "372", original_msg_type);
}

void FIXMessages::add_10(std::string& reply)
{
    auto checksum = fix::calculate_checksum(reply, false);
    add_tag_value(reply, "10", checksum);
}

} // namespace fix
