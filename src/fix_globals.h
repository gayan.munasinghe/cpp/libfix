#ifndef FIX_GLOBALS_H
#define FIX_GLOBALS_H

#include <map>
#include <mutex>

struct login
{
    bool logged_in;
    int sockfd;
    int last_message_time{0};
    int hb_interval{0};
};

struct sequence_number
{
    int incoming{1};
    int outgoing{1};
};

extern std::map<std::string, login> login_map_;
extern std::map<std::string, sequence_number> seq_map;
extern std::mutex login_map_mutex;

#endif
