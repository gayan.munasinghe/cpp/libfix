#include "fix_server.h"

#include <cstring>
#include <iomanip>
#include <iostream>
#include <iostream>
#include <regex>

#include <mix/globals.h>
#include <mix/log.h>
#include <mix/sockets.h>
#include <mix/utility.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include "fix_exception.h"
#include "fix_globals.h"
#include "fix_utility.h"
#include "fix_validate.h"

#define SOH "\x01"

namespace fix {

void Validater::initialize()
{
    mix::json::read_config(mix_config_file, config_);
    mix::json::read_config(mix::json::get_string(config_, "/users"), user_config_);
    mix::json::read_config(mix::json::get_string(config_, "/protocol"), fix_config_);
}

void Validater::validate(const std::string& msg)
{
    logger.add_log(LOG_DEBUG, "Validating message\n", 1);
    auto msg_type = fix::get_tag_value(msg, "35");
    if ("A" == msg_type) {
        validate_username(msg);
        validate_password(msg);
    }
    // validate sequence number only when reset sequence number is not set in a login message
    if (!(("A" == msg_type) && ("Y" == fix::get_tag_value(msg, "141")))) {
        validate_sequence_number(msg);
    }
    validate_tags(msg);
    validate_version(msg);
    validate_body_length(msg);
    validate_checksum(msg);
    validate_sendercompid(msg);
    validate_targetcompid(msg);
}

void Validater::validate_sequence_number(const std::string& msg)
{
    if (!fix::tag_exists(msg, "34")) {
        throw mix::fix::reject_3("Required tag missing", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__), "34", "1");
    }
    int sequence_number = std::stoi(fix::get_tag_value(msg, "34"));
    auto expected_sequence_number = seq_map[fix::get_tag_value(msg, "49")].incoming;
    if (sequence_number != expected_sequence_number) {
        logger.add_log(LOG_ERR, "Sequence number mismatch\n");
        throw mix::fix::reject_3_sequence_no("Sequence number mismatch, Expected " + std::to_string(expected_sequence_number) + " Received " + std::to_string(sequence_number),
             mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__), "34", "5");
    }
    else {
        logger.add_log(LOG_DEBUG, "Sequence number validated\n", 3);
    }
}

void Validater::validate_tags(const std::string& msg)
{
    validate_header_tags(msg);
    validate_body_tags(msg);
}

void Validater::validate_header_tags(const std::string& msg)
{
    validate_section_tags(msg, "header");
}

void Validater::validate_body_tags(const std::string& msg)
{
    auto msg_type = fix::get_tag_value(msg, "35");
    validate_section_tags(msg, msg_type);
}

void Validater::validate_trailer_tags(const std::string& msg)
{
    validate_section_tags(msg, "trailer");
}

void Validater::validate_section_tags(const std::string&msg, const std::string& section) {
    logger.add_log(LOG_DEBUG, "Validating " + section + "\n", 1);
    Value::ConstMemberIterator itr = fix_config_.FindMember(section.c_str());
    if (itr != fix_config_.MemberEnd()) {
        for (auto& it : itr->value.GetObject()) {
            std::string json_pointer = std::string("/") + section + "/" + it.name.GetString() + "/required";
            if (mix::json::get_string(fix_config_, json_pointer) == "Y") {
                if (!fix::tag_exists(msg, it.name.GetString())) {
                    throw mix::fix::reject_3("Required tag missing", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__), it.name.GetString(), "1");
                }
            }
        }
    }
}

void Validater::validate_body_length(const std::string& msg)
{
    auto sent_length = fix::get_tag_value(msg, "9");
    std::string reg_search = std::string(".*") + SOH + "9=(.*?)" + SOH + "(.*" + SOH + ")10=";
    std::regex rgx(reg_search);
    std::smatch matches;
    std::regex_search(msg, matches, rgx);
    int actual_length = matches[2].length();
    if (sent_length != std::to_string(actual_length)) {
        logger.add_log(LOG_DEBUG, "Invalid length\n", 1);
        throw mix::fix::reject_3("Invalid length", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__), "9", "5");
    }
    else {
        logger.add_log(LOG_DEBUG, "length validated\n", 1);
    }
}

void Validater::validate_username(const std::string& msg)
{
    auto sendercompid = fix::get_tag_value(msg, "49");
    auto username_sent = fix::get_tag_value(msg, "553");
    if (username_sent != mix::json::get_string(user_config_, "/" + sendercompid + "/username")) {
        logger.add_log(LOG_DEBUG, "Invalid username\n", 1);
        throw mix::fix::logout_response("Invalid username", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    }
    else {
        logger.add_log(LOG_DEBUG, "username validate\n", 1);
    }
}

void Validater::validate_password(const std::string& msg)
{
    auto sendercompid = fix::get_tag_value(msg, "49");
    auto password_sent = fix::get_tag_value(msg, "554");
    if (password_sent != mix::json::get_string(user_config_, "/" + sendercompid + "/password")) {
        logger.add_log(LOG_DEBUG, "Invalid password\n", 1);
        throw mix::fix::logout_response("Invalid password", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__));
    }
    else {
        logger.add_log(LOG_DEBUG, "password validated\n", 1);
    }
}

void Validater::validate_sendercompid(const std::string& msg)
{
    auto sendercompid = fix::get_tag_value(msg, "49");
    if (!mix::json::key_exists(user_config_, sendercompid)) {
        logger.add_log(LOG_ERR, "sendercompid " + sendercompid + " does not exist\n");
    }
    else {
        logger.add_log(LOG_DEBUG, "sendercompid " + sendercompid + " validated\n", 3);
    }
}

void Validater::validate_targetcompid(const std::string& msg)
{
    auto targetcompid = fix::get_tag_value(msg, "56");
    auto targetcompid_ex = mix::json::get_string(config_, "/sendercompid");
    if (!(targetcompid_ex == targetcompid)) {
        logger.add_log(LOG_ERR, "targetcompid " + targetcompid + " does not exist\n");
        throw mix::fix::reject_3("targetcompid " + targetcompid + " does not exist", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__), "56", "5");
    }
    else {
        logger.add_log(LOG_DEBUG, "targetcompid " + targetcompid + " validated\n", 3);
    }
}

void Validater::validate_version(const std::string& msg)
{
    std::string version = fix::get_tag_value(msg, "8");
    if (version != mix::json::get_string(config_, "/version")) {
        logger.add_log(LOG_ERR, "FIX version error\n");
        throw mix::fix::reject_3("FIX version mismatch", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__), "8", "5");
    }
    else {
        logger.add_log(LOG_DEBUG, "FIX version validated\n", 3);
    }
}

void Validater::validate_checksum(const std::string& msg)
{
    std::string check_sum = fix::calculate_checksum(msg);
    std::string check_sum_msg = fix::get_tag_value(msg, "10");
    if (check_sum != check_sum_msg) {
        logger.add_log(LOG_ERR, "Checksum error\n");
        throw mix::fix::reject_3("Checksum error", mix::strings::where(__LINE__, __FILE__, __PRETTY_FUNCTION__), "10", "5");
    }
    else {
        logger.add_log(LOG_DEBUG, "checksum validated\n", 3);
    }
}

} //end namespace fix
