#include <iostream>

#include "fix_utility.h"
#include "orderbook.h"

void Orderbook::process_order(const std::string& msg, const std::string& timestamp)
{
    auto message_type = fix::get_tag_value(msg, "35");
    if ("D" == message_type) {
        add_order(msg, timestamp);
    }
    else if ("F" == message_type) {
        cancel_order(msg, timestamp);
    }
    else if ("G" == message_type) {
        replace_order(msg, timestamp);
    }
}

void Orderbook::add_order(const std::string& msg, const std::string& timestamp)
{
    auto price = std::stoi(fix::get_tag_value(msg, "44"));
    auto qty = std::stoi(fix::get_tag_value(msg, "38"));
    auto clordid = fix::get_tag_value(msg, "11");
    order o{timestamp, clordid, price, qty};
    auto side = fix::get_tag_value(msg, "54");
    if ("1" == side) {
        buybook_.insert(std::make_pair(price*(-1), o));
    }
    else if ("2" == side) {
        sellbook_.insert(std::make_pair(price, o));
    }
    print();
}

void Orderbook::cancel_order(const std::string& msg, const std::string& timestamp)
{
    auto orig_clordid = fix::get_tag_value(msg, "41");
    auto side = fix::get_tag_value(msg, "54");
    if ("1" == side) {
        for (auto it = buybook_.begin(); it != buybook_.end(); ++it) {
            if (it->second.clordid == orig_clordid) {
                buybook_.erase(it);
                break;
            }
        }
    }
    else if ("2" == side) {
        for (auto it = sellbook_.begin(); it != sellbook_.end(); ++it) {
            if (it->second.clordid == orig_clordid) {
                sellbook_.erase(it);
                break;
            }
        }
    }
    print();
}

void Orderbook::replace_order(const std::string& msg, const std::string& timestamp)
{
    cancel_order(msg, timestamp);
    add_order(msg, timestamp);
}

void Orderbook::print()
{
    for (auto it = sellbook_.rbegin(); it != sellbook_.rend(); ++it) {
        std::cout<<it->second.timestamp
                 <<" "<<it->second.clordid
                 <<" "<<it->second.price
                 <<" "<<it->second.qty
                 <<std::endl;
    }
    std::cout<<"---------"<<std::endl;
    for (auto it : buybook_) {
        std::cout<<it.second.timestamp
                 <<" "<<it.second.clordid
                 <<" "<<it.second.price
                 <<" "<<it.second.qty
                 <<std::endl;
    }
}
