#ifndef FIX_UTILITY_H
#define FIX_UTILITY_H

#include <regex>
#include <string>
#include <tuple>

namespace fix {

std::string get_tag_value(const std::string& msg, const std::string& tag);
std::string get_search_string(const std::string& tag);
int get_checksum_int(const std::string& s);
bool tag_exists(const std::string& msg, const std::string& tag);
std::tuple<bool, std::smatch> get_match(const std::string& msg, const std::string& tag);
void reset_sequence_numbers(const std::string& session);
std::string calculate_checksum(const std::string& msg, bool with_trailer = true);
std::string get_checksum_string(const std::string& msg, bool with_trailer = true);
int get_sequence_number(const std::string& file_name);

} // end namespace fix

#endif
