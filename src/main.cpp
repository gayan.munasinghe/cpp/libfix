#include <cstdio>
#include <iostream>

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <mix/exception.h>
#include <mix/globals.h>
#include <mix/log.h>

#include "fix_server.h"
#include "fix_globals.h"

std::string mix_config_file;

void get_options(int argc, char** argv)
{
    int option;
    while ((option = getopt (argc, argv, "c:")) != -1) {
        switch (option) {
        case 'c':
            mix_config_file = optarg;
            break;
        case '?':
            exit(EXIT_FAILURE);
        default:
            exit(EXIT_FAILURE);
        }
    }
}

int main(int argc, char** argv) {
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    try {
        mix_config_file = "cfg/fixdora.json";
        get_options(argc, argv);
        FIXServer fix_server;
        fix_server.initialize();
        fix_server.run_server("2018");
    }
    catch (const mix::key_error& e) {
        logger.add_log(LOG_ERR, std::string(e.where()) + " : key_error : key " + e.what() +"\n");
    }
    catch (const mix::mix_exception& e) {
        logger.add_log(LOG_ERR, std::string(e.where()) + " " + e.what() +"\n");
    }
    return 0;
}
