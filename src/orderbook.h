#ifndef ORDERBOOK_H
#define ORDERBOOK_H

#include <cstdint>
#include <map>
#include <string>

struct order
{
    std::string timestamp;
    std::string clordid;
    int price;
    int qty;
};

class Orderbook
{
public:
    void process_order(const std::string& msg, const std::string& timestamp);
private:
    void add_order(const std::string& msg, const std::string& timestamp);
    void cancel_order(const std::string& msg, const std::string& timestamp);
    void replace_order(const std::string& msg, const std::string& timestamp);
    std::multimap<int, order> sellbook_;
    std::multimap<int, order> buybook_;
    void print();
};

#endif
